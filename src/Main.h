#pragma once

#include "FunctionScanner.h"
#include "Translator.h"
#include "XmlFormatter.h"

#include "tclap/CmdLine.h"

using std::string;
using std::vector;

using ApiScan::FunctionScanner;
using ApiScan::FunctionInfo;
using ApiScan::ParameterInfo;
using ApiScan::Translator;
using ApiScan::XmlFormatter;

using TCLAP::CmdLine;
using TCLAP::UnlabeledMultiArg;
